import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { User, AcntType, BillingStatus, Authentication, Email } from '../model/User.model';
import * as $ from 'jquery';
import { Alert } from 'selenium-webdriver';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
public userList: User[];
public users: User = new User();
 public aType: typeof AcntType = AcntType;
 public bStatus: typeof BillingStatus  = BillingStatus;
 public auth: typeof Authentication = Authentication;

 fName: string;
 a: string;
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.getUserList();
  }
getWOwner() {
  this.userService.GetUserbyacntType(1).subscribe((data) => {this.userList = data;
    console.log(data);
  });
}
getWAdmin() {
  this.userService.GetUserbyacntType(2).subscribe((data) => {this.userList = data;
    console.log(data);
  });
}
getFMember() {
  this.userService.GetUserbyacntType(3).subscribe((data) => {this.userList = data;
    console.log(data);
  });
}
getGuest() {
  this.userService.GetUserbyacntType(4).subscribe((data) => {this.userList = data;
    console.log(data);
  });
}
getActive() {
  this.userService.GetUserbybillingStatus(1).subscribe((data) => {this.userList = data;
    console.log(data);
  });
}
getInactive() {
  this.userService.GetUserbybillingStatus(2).subscribe((data) => {this.userList = data;
    console.log(data);
  });
}

getUserList() {
this.userService.GetAllUser().subscribe((data) => {this.userList = data;
console.log(data); });
}
getUserbyfirstName() {
  // alert('sadas');
  this.a = $('#typeSearch').val();
 this.userService.GetUserbyfirstName(this.a).subscribe((data) => {this.userList = data;
  console.log(data);
});
}
getUserbyacntType(acntType) {
  this.userService.GetUserbyacntType(acntType).subscribe((data) => {this.userList = data;
    console.log(data);
  });
}
getUserbybillingStatus(billingStatus) {
  this.userService.GetUserbybillingStatus(billingStatus).subscribe((data) => {this.userList = data;
    console.log(data);
  });
}
getUserbyauthentication(authentication) {
  this.userService.GetUserbyAuthentication(authentication).subscribe((data) => {this.userList = data;
    console.log(data);
  });
}

changeAdmin(user: User) {

  this.userService.UpdateAdmin(user).subscribe((data) => {
    console.log(data);
    this.getUserList();
  });
}

changeGuest(user: User) {
  this.userService.UpdateGuest(user).subscribe((data) => {
    console.log(data);
    this.getUserList();
  });
}
changeStatus(user: User) {
  this.userService.deactivate(user).subscribe((data) => {
    console.log(data);
    this.getUserList();
  });
}
}
