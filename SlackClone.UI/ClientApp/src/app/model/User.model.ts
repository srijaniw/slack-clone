export class User {
  id: number;
  userName: string;
  firstName: string;
  lastName: string;
  email: string;
  acntType: AcntType;
  billingStatus: BillingStatus;
  authentication: Authentication;
 }

 export class Email {
   toAddress: string;
   fromAdress: string;
   Subject: string;
  Content: string;
 }
 export enum AcntType {
  WOwner = 1,
  WAdmin = 2,
  FMember = 3,
  Guest = 4
 }

 export enum BillingStatus {
  Active = 1,
  Inactive = 2
 }

 export enum Authentication {
  Default = 1,
  TwoFActor = 2,
  SSO = 3
 }
