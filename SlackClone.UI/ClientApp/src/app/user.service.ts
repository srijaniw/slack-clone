import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';
import { User, Email } from './model/User.model';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UserService {

  constructor(private httpClient: HttpClient) { }
  public GetAllUser(): Observable<User[]> {
    return this.httpClient.get<User[]>(`${environment.apiurl}/User`);
   }
   public GetUserbyfirstName(firstName): Observable<User[]> {
    return this.httpClient.get<User[]>(`${environment.apiurl}/User/GetuserbyfirstName/${firstName}`);
   }
   public GetUserbyacntType(acntType): Observable<User[]> {
    return this.httpClient.get<User[]>(`${environment.apiurl}/User/GetbyAcntType/${acntType}`);
   }
   public GetUserbybillingStatus(billingStatus): Observable<User[]> {
    return this.httpClient.get<User[]>(`${environment.apiurl}/User/GetbyBillingStatus/${billingStatus}`);
   }
   public GetUserbyAuthentication(authentication): Observable<User[]> {
    return this.httpClient.get<User[]>(`${environment.apiurl}/User/GetbyAuthentication/${authentication}`);
   }
   public SendInvitation(emailList) {
     console.log(emailList);
    return this.httpClient.post(`${environment.apiurl}/User/SendEmail/`, emailList);
  }

  public UpdateAdmin(user) {
    return this.httpClient.put(`${environment.apiurl}/User/UpdateAdmin`, user);
  }
  public UpdateGuest(user) {
    return this.httpClient.put(`${environment.apiurl}/User/UpdateGuest`, user);
  }
  public deactivate(user) {
    return this.httpClient.put(`${environment.apiurl}/User/Deactivate`, user);
  }
}
