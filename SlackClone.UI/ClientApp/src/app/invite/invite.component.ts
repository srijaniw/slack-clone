import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Email } from '../model/User.model';

@Component({
  selector: 'app-invite',
  templateUrl: './invite.component.html',
  styleUrls: ['./invite.component.css']
})
export class InviteComponent implements OnInit {

  public emails: Email = new Email();
  public items: Array<number> = [];
  public newAttribute: number ;
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.emails.fromAdress = 'srijaniw@gmail.com';
    this.emails.Subject = 'Slack Invitation';
    this.emails.Content = 'Please Join Slack!';
  }
  addFieldValue() {
    this.items.push(1);
}
deleteFieldValue(index) {
  this.items.splice(index, 1);
}
sendInvitation(sendInvitaionForm) {
  const emailList = [];
  if (sendInvitaionForm.email0 !== undefined) {
    emailList.push(sendInvitaionForm.email0);
  }
  if (sendInvitaionForm.email1 !== undefined) {
    emailList.push(sendInvitaionForm.email1);
  }
  if (sendInvitaionForm.email2 !== undefined) {
    emailList.push(sendInvitaionForm.email2);
  }
  if (sendInvitaionForm.email3 !== undefined) {
    emailList.push(sendInvitaionForm.email3);
  }
  if (sendInvitaionForm.email4 !== undefined) {
    emailList.push(sendInvitaionForm.email4);
  }
  if (sendInvitaionForm.email5 !== undefined) {
    emailList.push(sendInvitaionForm.email5);
  }
  if (sendInvitaionForm.email6 !== undefined) {
    emailList.push(sendInvitaionForm.email6);
  }
  if (sendInvitaionForm.email7 !== undefined) {
    emailList.push(sendInvitaionForm.email7);
  }
  if (sendInvitaionForm.email8 !== undefined) {
    emailList.push(sendInvitaionForm.email8);
  }
  if (sendInvitaionForm.email9 !== undefined) {
    emailList.push(sendInvitaionForm.email9);
  }

  this.userService.SendInvitation(emailList).subscribe(res => {
    alert('send successfully');
  });
}
}
