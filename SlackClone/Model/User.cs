﻿using SlackClone.Model.Enum;

namespace SlackClone.Model
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public AcntType AcntType { get; set; }
        public BillingStatus BillingStatus { get; set; }
        public Authentication Authentication { get; set; }
    }
}
