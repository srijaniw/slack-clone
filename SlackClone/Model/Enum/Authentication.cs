﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SlackClone.Model.Enum
{
    public enum Authentication
    {
        Default=1,
       TwoFActor,
       SSO
    }
}
