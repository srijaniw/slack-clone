﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SlackClone.Model;
using SlackClone.Model.Enum;
using SlackClone.Service;

namespace SlackClone.Controllers
{
    [EnableCors("SlackClone")]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly SlackCloneContext _ctxt;
      private readonly MailService _emailService;
        public UserController(SlackCloneContext context ,MailService emailService) {
            _ctxt = context;
             _emailService = emailService;
        }

        [HttpGet]
        public IEnumerable<User> GetAllUser() {
          
                return _ctxt.Users;  
        }
        [HttpGet("GetuserbyfirstName/{firstName}")]
        public IEnumerable<User> GetUserbyfirstName(string firstName)
        {
           // var result = _ctxt.Users.Where(e => e.FirstName == firstName);
            var result = _ctxt.Users.Where(e => e.FirstName.ToLower().Contains(firstName.ToLower()));
            return result;
        }
        [HttpGet("GetbyAcntType/{acntType}")]
        public IEnumerable<User> GetUserbyAcntType(int acntType )
        {
            var aType = (AcntType)acntType;
            var result = _ctxt.Users.Where(e => e.AcntType == aType);
            return result;
        }

        [HttpGet("GetbyBillingStatus/{billingStatus}")]
        public IEnumerable<User> GetUserbyBillingStatus(int billingStatus)
        {
            var bStatus = (BillingStatus)billingStatus;
            var result = _ctxt.Users.Where(e => e.BillingStatus == bStatus);
            return result;
        }
        [HttpGet("GetbyAuthentication/{authentication}")]
        public IEnumerable<User> GetUserbyAuthentication(int authentication)
        {
            var auth = (Authentication)authentication;
            var result = _ctxt.Users.Where(e => e.Authentication == auth);
            return result;
        }


    
        [HttpPost("SendEmail")]
        public ActionResult SendInvitation([FromBody] string[] emailList)
        {
            _emailService.SendInvitation(emailList);
            return Ok();
        }

        [HttpPut("UpdateAdmin")]

        public User UpdateAdmin([FromBody] User newUser)
        {
            User dbUser = _ctxt.Users.Where(e => e.Id == newUser.Id).SingleOrDefault();
            dbUser.FirstName = newUser.FirstName;
            dbUser.LastName = newUser.LastName;
            dbUser.UserName = newUser.UserName;
            dbUser.Email = newUser.Email;
            dbUser.AcntType = (AcntType)2;
            dbUser.BillingStatus = newUser.BillingStatus;
            dbUser.Authentication = newUser.Authentication;
            _ctxt.SaveChanges();
            return newUser;
        }

        [HttpPut("UpdateGuest")]

        public User UpdateGuest([FromBody] User newUser)
        {
            User dbUser = _ctxt.Users.Where(e => e.Id == newUser.Id).SingleOrDefault();
            dbUser.FirstName = newUser.FirstName;
            dbUser.LastName = newUser.LastName;
            dbUser.UserName = newUser.UserName;
            dbUser.Email = newUser.Email;
            dbUser.AcntType = (AcntType)4;
            dbUser.BillingStatus = newUser.BillingStatus;
            dbUser.Authentication = newUser.Authentication;
            _ctxt.SaveChanges();
            return newUser;
        }

        [HttpPut("Deactivate")]

        public User updateStatus([FromBody] User newUser)
        {
            User dbUser = _ctxt.Users.Where(e => e.Id == newUser.Id).SingleOrDefault();
            dbUser.FirstName = newUser.FirstName;
            dbUser.LastName = newUser.LastName;
            dbUser.UserName = newUser.UserName;
            dbUser.Email = newUser.Email;
            dbUser.AcntType = newUser.AcntType;
            dbUser.BillingStatus = (BillingStatus)2;
            dbUser.Authentication = newUser.Authentication;
            _ctxt.SaveChanges();
            return newUser;
        }
    }
}