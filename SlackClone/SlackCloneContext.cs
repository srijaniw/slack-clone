﻿using Microsoft.EntityFrameworkCore;
using SlackClone.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SlackClone
{
    public class SlackCloneContext:DbContext
    {
        public SlackCloneContext(DbContextOptions<SlackCloneContext> options) : base(options) { }
        public virtual DbSet<User> Users { get; set; }
    }
   
}
