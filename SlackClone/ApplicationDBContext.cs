﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System.Reflection;

namespace SlackClone
{
    public class ApplicationDBContext : IDesignTimeDbContextFactory<SlackCloneContext>
    {
        public SlackCloneContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<SlackCloneContext>();
            builder.UseSqlServer("Server=.; Database=SlackClone;Trusted_Connection=True",
                optionsBUilder => optionsBUilder.MigrationsAssembly(typeof(SlackCloneContext).GetTypeInfo().Assembly.GetName().Name));
            return new SlackCloneContext(builder.Options);
        }

    }
}
